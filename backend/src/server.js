const app = require("./app");
const connectDatabase = require("./db/Database.js");
const cloudinary = require("cloudinary");
// Handling uncaught Exception
process.on("uncaughtException",(err) =>{
    console.log(`Error: ${err.message}`);
    console.log(`Shutting down the server for Handling uncaught Exception`);
})

// config
require("dotenv").config();
// connect database
connectDatabase();

cloudinary.config({
    cloud_name: 'check-media', 
  api_key: '186999341655511', 
  api_secret: 'Idu0UKKWfMzPRCzGp772T0wHPSI' 
})

// create server
const server = app.listen(process.env.PORT,() =>{
    console.log(`Server is working on http://localhost:${process.env.PORT}`)
})


// Unhandled promise rejection
process.on("unhandledRejection", (err) =>{
    console.log(`Shutting down server for ${err.message}`);
    console.log(`Shutting down the server due to Unhandled promise rejection`);
    server.close(() =>{
        process.exit(1);
    });
});