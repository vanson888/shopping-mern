// const { body, validationResult } = require('express-validator');
const { body } = require('express-validator/check')

exports.validate = (method) => {
  switch (method) {
    case 'createUser': {
        console.log(43434)
     return [ 
        body('name','User name is required').not().isEmpty().isLength({ min: 5 }).withMessage('Name must be at least 5 chars long'),
        body('email', 'Invalid email').isEmail(),
        body('password').isLength({
            min: 8
          })
          .withMessage("Password must contain at least 8 characters")
          .isLength({
            max: 20
          })
          .withMessage("Password can contain max 20 characters")
          .matches(/(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, "i")
          .withMessage("Passwords invalid."),
          body('avatar', 'Avatar is required ').not().isEmpty(),
       ]   
    }
    case 'loginUser': {
   return [ 
      body('email', 'Invalid email').not().isEmpty().isEmail(),
      body('password', 'Password is required').not().isEmpty()
     ]   
  }
  }
}