const catchAsyncErrors = require("../middleware/catchAsyncErrors");

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

exports.Payment = catchAsyncErrors(async (req, res, next) => {
  try {
    const Stripe = require('stripe');
    const stripes = Stripe(process.env.STRIPE_SECRET_KEY);
    const myPayment = await stripes.paymentIntents.create({
      amount: req.body.amount,
      currency: "usd",
      metadata: {
        company: "MERN",
      },
      payment_method_types: ['card'],
    });

    res
      .status(200)
      .json({ success: true, client_secret: myPayment.client_secret });
  } catch (error) {
    console.log(error, 445)
  }

});

exports.sendStripeApiKey = catchAsyncErrors(async (req, res, next) => {
  res.status(200).json({ stripeApiKey: process.env.STRIPE_API_KEY });
});
