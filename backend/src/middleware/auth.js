const ErrorHandler = require("../utils/ErrorHandler");
const catchAsyncErrors = require("./catchAsyncErrors");
const jwt = require("jsonwebtoken");
const User = require("../models/UserModel");


exports.isAuthenticatedUser = catchAsyncErrors(async (req, res, next) => {
  if (req.headers.authorization) {
    const token = req.headers.authorization.split(" ")[1];
    console.log(3434, process.env.JWT_SECRET_KEY, token);
    const decodedData = jwt.verify(token, process.env.JWT_SECRET_KEY);
  
    req.user = await User.findById(decodedData.id);
    next();
  } else {
    return next(new ErrorHandler("Please Login for access this resource", 401));
  }
 
});

// Admin Roles
exports.authorizeRoles = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(new ErrorHandler(`${req.user.role} can not access this resources`));
    };
    next();
  }
}